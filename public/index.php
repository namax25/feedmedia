<?php
require_once __DIR__ . '/../vendor/autoload.php';

// all such parameters should be in a separate local config file or in env variables
$host = 'c_db';
$db = 'maindb';
$user = 'root';
$pass = '1';

$dsn = "mysql:host=$host;dbname=$db;";
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());

}

$widgetService = new \Service\WidgetService($pdo);
$widgets = $widgetService->findWidgetsWithTag('tag2', 0, 2);

//Here should be a render class
var_dump($widgets);