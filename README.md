# Widgets Service

PHP 7.4; MySQL; NGINX; 

### Install with docker

1 Clone Repository

```bash
git clone https://namax25@bitbucket.org/namax25/feedmedia.git
```

2 Enter to the folder

```bash
cd feedmedia
```

3 Run docker

```bash
docker-compose  -p feedmedia  -f docker-compose.yml up
```

4 Run composer

```bash
docker exec -it feedmedia_c_php_1  composer install
```


5 Import SQL dump
```bash
docker exec -i <DOCKER CONTAINER NAME> mysql -u<DB USER> -p<DB PASSWORD> <DATABASE NAME> < path/to/file.sql
```

6 Go to localhost in your browser or if you run it as a cli app, then you can just run 

```bash
 docker exec -it feedmedia_c_php_1 php public/index.php
```

### Install without docker

1. Place the code where it can be executed 
2. Adjust public/index.php file (add credentials to your database)
3. If you want run it as a web app, then tou need setup some server (nginx, apache) and virtual host
4. If you want run it as a cli app, then you can run 

```bash
php public/index.php
```



 