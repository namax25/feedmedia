<?php

namespace Dto;

/**
 * Class Widget
 * @package Dto
 */
class Widget
{
    /**
     * @var int
     */
    protected int $id;

    /**
     * @var string
     */
    protected string $name;

    /**
     * @var bool
     */
    protected bool $isDeleted;

    /**
     * All widget tags
     * @var array
     */
    protected array $tags = [];

    /**
     * All widget dongles
     * @var array
     */
    protected array $dongles = [];


    /**
     * Set properties values from array
     * @param array $data
     */
    public function fromArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = (int)$data['id'];
        }

        if (isset($data['name'])) {
            $this->name = (string)$data['name'];
        }

        if (isset($data['deleted'])) {
            $this->isDeleted = (bool)$data['deleted'];
        }

        if (isset($data['tags'])) {
            $this->tags = (array)$data['tags'];
        }

        if (isset($data['dongles'])) {
            $this->dongles = (array)$data['dongles'];
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Widget
     */
    public function setId(int $id): Widget
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Widget
     */
    public function setName(string $name): Widget
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * @param bool $isDeleted
     * @return Widget
     */
    public function setIsDeleted(bool $isDeleted): Widget
    {
        $this->isDeleted = $isDeleted;
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return Widget
     */
    public function setTags(array $tags): Widget
    {
        $this->tags = $tags;
        return $this;
    }

    /**
     * @return array
     */
    public function getDongles(): array
    {
        return $this->dongles;
    }

    /**
     * @param array $dongles
     * @return Widget
     */
    public function setDongles(array $dongles): Widget
    {
        $this->dongles = $dongles;
        return $this;
    }
}
