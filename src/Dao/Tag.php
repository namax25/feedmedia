<?php


namespace Dao;

/**
 * Class Dongle
 * @package Dao
 */
class Tag extends BaseDao
{

    /**
     * Return all dongles for all widgets ids
     * @param array $widgetIds
     * @return array
     */
    public function findByWidgetIds(array $widgetIds): array
    {
        $count = count($widgetIds);
        if (!$count) {
            return [];
        }

        $sql = <<<SQL
SELECT *
FROM widget_tag_map wdm
INNER JOIN tag t on wdm.tag_id = t.id
WHERE wdm.widget_id in (%s)
SQL;

        $sql = sprintf($sql, $this->generateQuestionMarks($widgetIds));
        $stmt = $this->db->prepare($sql);
        $stmt->execute($widgetIds);
        $result = [];
        while ($row = $stmt->fetch()) {
            if (!isset($result[$row['widget_id']])) {
                $result[$row['widget_id']] = [];
            }
            $result[$row['widget_id']][] = $row;
        }
        return $result;
    }
}
