<?php


namespace Dao;


use PDO;

/**
 *
 * Class Widget
 * @package Dao
 */
class Widget extends BaseDao
{
    /**
     * Return all active widgets with specific tag name
     * @param string $tagName
     * @return array|int[]
     */
    public function findByTagName(string $tagName): array
    {
        $sql = <<<SQL
SELECT w.id, w.name, w.deleted
FROM tag as t
         INNER JOIN widget_tag_map wtm on t.id = wtm.tag_id
         INNER JOIN widget as w ON w.id = wtm.widget_id
WHERE t.tag =:tag
  and w.deleted = 0
ORDER BY w.name ASC
LIMIT :offset, :max
SQL;

        $offset = $this->getOffset();
        $max = $this->getMax();
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':tag', $tagName, PDO::PARAM_STR);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->bindParam(':max', $max, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}
