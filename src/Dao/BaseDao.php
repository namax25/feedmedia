<?php


namespace Dao;


use PDO;

/**
 * Class BaseDao
 * @package Dao
 */
class BaseDao
{
    /**
     * Default value for SQL offset parameter
     */
    protected const DEFAULT_OFFSET = 0;

    /**
     * Default value for SQL row count parameter
     */
    protected const DEFAULT_MAX_LIMIT = 10;

    /**
     * DB connection compatible with PDO interface
     * @var PDO
     */
    protected PDO $db;

    /**
     * SQL offset parameter in LIMIT
     * @var int
     */
    protected int $offset = self::DEFAULT_OFFSET;

    /**
     * SQL row count parameter in LIMIT
     * @var int
     */
    protected int $max = self::DEFAULT_MAX_LIMIT;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Return ?, ?, ... ? string depends on amount of values in the data array. It need for PDO IN statement
     * @param array $data
     * @return string
     */
    protected function generateQuestionMarks(array $data): string
    {
        return str_repeat("?, ", count($data) - 1) . "?";
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return BaseDao
     */
    public function setOffset(int $offset): BaseDao
    {
        $this->offset = $offset < 0 ? self::DEFAULT_OFFSET : $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax(): int
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return BaseDao
     */
    public function setMax(int $max): BaseDao
    {
        $this->max = $max < 0 ? self::DEFAULT_MAX_LIMIT : $max;;
        return $this;
    }
}