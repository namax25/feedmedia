<?php

namespace Service;

use PDO;

/**
 * The service works with widgets. Also you able to get all related info about widgets (eg tags, dongles)
 * Class WidgetService
 * @package Service
 */
class WidgetService
{

    /**
     * DB connection compatible with PDO interface
     * @var PDO
     */
    protected PDO $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * Search all widgets by the tag name. Will return all widgets with all tags, dongles for each widget.
     * @param string $tagName
     * @param int $offset
     * @param int $max
     * @return \Dto\Widget[]
     */
    public function findWidgetsWithTag(string $tagName, int $offset, int $max): array
    {
        $result = [];
        $widgetDao = new \Dao\Widget($this->db);
        $widgetDao->setOffset($offset)
            ->setMax($max);
        $widgets = $widgetDao->findByTagName($tagName);

        $widgetIds = [];
        foreach ($widgets as $widget) {
            $widgetIds[] = $widget['id'];
        }

        $dongles = $this->findAllDonglesByWidgetsIds($widgetIds);
        $tags = $this->findAllTagsByWidgetsIds($widgetIds);

        foreach ($widgets as $widgetData) {
            $widget = new \Dto\Widget();
            $widget->fromArray($widgetData);
            if (!empty($dongles[$widgetData['id']])) {
                $widget->setDongles($dongles[$widgetData['id']]);
            }
            if (!empty($tags[$widgetData['id']])) {
                $widget->setTags($tags[$widgetData['id']]);
            }
            $result[] = $widget;
        }
        return $result;
    }

    /**
     * Search all tags by widgets ids
     * @param array $widgetIds
     * @return array
     */
    public function findAllTagsByWidgetsIds(array $widgetIds): array
    {
        $tagDao = new \Dao\Tag($this->db);
        return $tagDao->findByWidgetIds($widgetIds);
    }

    /**
     * Search all dongles by widgets ids
     * @param array $widgetIds
     * @return array
     */
    public function findAllDonglesByWidgetsIds(array $widgetIds): array
    {
        $dongleDao = new \Dao\Dongle($this->db);
        return $dongleDao->findByWidgetIds($widgetIds);
    }

}